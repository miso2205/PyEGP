import xml.etree.ElementTree as ET
from collections import defaultdict
from anytree import Node, RenderTree, AsciiStyle
import pprint
import tempfile
import zipfile
import shutil
import os
import os.path
import sys
from operator import itemgetter
import traceback

_pp = pprint.PrettyPrinter(indent=4)

def _etree_to_dict(t):
    d = {t.tag: {} if t.attrib else None}
    children = list(t)
    if children:
        dd = defaultdict(list)
        for dc in map(_etree_to_dict, children):
            for k, v in dc.items():
                dd[k].append(v)
        d = {t.tag: {k: v[0] if len(v) == 1 else v
                     for k, v in dd.items()}}
    if t.attrib:
        d[t.tag].update(('@' + k, v)
                        for k, v in t.attrib.items())
    if t.text:
        text = t.text.strip()
        if children or t.attrib:
            if text:
              d[t.tag]['#text'] = text
        else:
            d[t.tag] = text
    return d

def _extract_node_info(node_id, root, project_unpacked_folder):
#    print("extract_node_info:["+node_id+']')
    elements = root.findall(f".//*[ID='{node_id}']/..")
    egp_PDF = root.findall(f".//NoteGraphic")
    element_offset = 0
    for elem in egp_PDF:
        egp_PDF_d = _etree_to_dict(elem)
#        _pp.pprint(egp_PDF_d)
        if egp_PDF_d['NoteGraphic']['Element'] == node_id:
            element_offset = int(egp_PDF_d['NoteGraphic']['PosY'])
#        else:
#            print(f"{egp_PDF_d['NoteGraphic']['Element']} <> {node_id}")
            
#    print(str(elements))
    node_info = {}
    for elem in elements:
        d = _etree_to_dict(elem)
#        _pp.pprint(d)
        try:
#            print(d['Element']['Element']['Label'])
            node_info['index'] = element_offset
            node_info['label'] = d['Element']['Element']['Label']
            node_info['type'] = d['Element']['Element']['Type']
            
#            print(project_unpacked_folder+'/'+node_id+'/'+'code.sas')
            if os.path.isfile(project_unpacked_folder+'/'+node_id+'/'+'code.sas'):
                node_info['code'] = open(project_unpacked_folder+'/'+node_id+'/'+'code.sas', 'r', encoding='utf-8-sig').read()
#                print(node_info['code'])
            else:
                code_id = ''
                code_id = d['Element']['SubmitableElement']['JobRecipe']['JobRecipe']['code']
#                print('code_id:['+code_id+']')
                code_elements = root.findall(f".//*[ID='{code_id}']/..")
#                print(code_elements)
                if code_elements:
                    code_d = _etree_to_dict(code_elements[0])
        #            pp.pprint(code_d)
                    node_info['code'] = code_d['Element']['TextElement']['Text']
        except KeyError as e:
            if str(e) != "'Element'" and str(e) != "'SubmitableElement'": print('KeyError:'+str(e))
                
    return node_info

class PyEGP:
    __unpacked_egp_folder = None
    __root_id_list = []
    __main_root_id = None
    __nodes_info = None
    __import_tasks = []
    
    def __init__(self, sas_egp_file_name, manual_root_node_label=None):
        print("epg file:["+sas_egp_file_name+"]")
        dir_name, file_name = os.path.split(sas_egp_file_name)
        print('temp folder:['+tempfile.gettempdir()+']')
        shutil.rmtree(tempfile.gettempdir()+'/'+file_name, ignore_errors=True)
        os.makedirs(tempfile.gettempdir()+'/'+file_name)
        with zipfile.ZipFile(sas_egp_file_name, 'r') as zip_ref:
            zip_ref.extractall(tempfile.gettempdir()+'/'+file_name)
            
        self.__unpacked_egp_folder = tempfile.gettempdir()+'/'+file_name
        self.__tree = ET.parse(self.__unpacked_egp_folder+'/project.xml')
        
        self.manual_root_node_label = manual_root_node_label
        self.__root_id_list = []
        self.__main_root_id = None
        self.__nodes_info = None
        self.__import_tasks = []
#        ET.dump(self.__tree)
        
    def __scan_egp_file(self):
        root = self.__tree.getroot()
        
        elements = root.findall(".//Element[@Type='SAS.EG.ProjectElements.ImportTask']")
#        print(str(elements))
        for element in elements:
            lib_name = None
            table_name = None
            full_path = None
            id = None
            column_mapping = {}
            output_data_shortcut = None
            
            d = _etree_to_dict(element)
#            _pp.pprint(d)
            try:
                lib_name = d['Element']['SubmitableElement']['ExpectedOutputDataList']['DataDescriptor']['LibraryName']
                table_name = d['Element']['SubmitableElement']['ExpectedOutputDataList']['DataDescriptor']['Member']
                id = d['Element']['Element']['ID']
                shortcut_id = d['Element']['Element']['InputIDs']
                external_files = root.findall(f".//ExternalFile/ShortCutList/[ShortCutID='{shortcut_id}']/../..")
                if d['Element']['SubmitableElement']['JobRecipe']['JobRecipe']['OutputDataList']:
#                    _pp.pprint(d['Element']['SubmitableElement'])
                    output_data_shortcut = d['Element']['SubmitableElement']['JobRecipe']['JobRecipe']['OutputDataList']['ID']
                    for external_file in external_files:
                        ef = _etree_to_dict(external_file)
#                        _pp.pprint(ef['ExternalFile']['ExternalFile'])
                        ef_info = _etree_to_dict(ET.fromstring(ef['ExternalFile']['ExternalFile']['DNA']))
#                        _pp.pprint(ef_info)
                        full_path = ef_info['DNA']['FullPath']
                        break
            
            except KeyError as e:
                pass
            except Exception as e:
                print(f'EXCEPTION ({type(e).__name__}):'+str(e))
                print(traceback.format_exc())
                exit(0)
                pass
                    
            if id and output_data_shortcut:
                inport = ET.parse(self.__unpacked_egp_folder+f'/{id}/{id}.xml')
                import_root = inport.getroot()
                elements = import_root.findall(".//MainData")
#                _pp.pprint(elements)
                for e in elements:
                    e_dict = _etree_to_dict(e)
#                    _pp.pprint(e_dict)
                    if isinstance(e_dict['MainData']['ImportDataClass']['FieldsInfoData']['FieldInfo'], dict):
                        column_mapping[e_dict['MainData']['ImportDataClass']['FieldsInfoData']['FieldInfo']['Label']] = e_dict['MainData']['ImportDataClass']['FieldsInfoData']['FieldInfo']['Format']
                    else:
                        for e1 in e_dict['MainData']['ImportDataClass']['FieldsInfoData']['FieldInfo']:
    #                        _pp.pprint(e1)
                            column_mapping[e1['Label']] = e1['Format']
                    break
                
                self.__import_tasks.append({'lib_name': lib_name, 'table_name': table_name, 'full_path': full_path, 'id': id, 'column_mapping': column_mapping, 'input_id': shortcut_id, 'output_id': output_data_shortcut})
#        print("TRACE:6", self.__import_tasks)

        root = self.__tree.getroot()
        elements = root.findall(".//Element[@Type='SAS.EG.ProjectElements.Link']")
#        print(str(elements))
        from_list = []
        to_list = []
        nodes_info = {}
        tree_nodes = []
        for element in elements:
            d = _etree_to_dict(element)
            try:
#                _pp.pprint(d)
                if d['Element']['Log']['LinkFrom'] and d['Element']['Log']['LinkTo']:
#                    print("TRACE:2", d['Element']['Log']['LinkFrom'], '->', d['Element']['Log']['LinkTo'])
                    if d['Element']['Log']['LinkFrom'] not in from_list: from_list.append(d['Element']['Log']['LinkFrom'])
                    if d['Element']['Log']['LinkTo'] not in to_list: to_list.append(d['Element']['Log']['LinkTo'])
                    
                    if d['Element']['Log']['LinkFrom'] not in nodes_info:
                        node_info = _extract_node_info(d['Element']['Log']['LinkFrom'], root, self.__unpacked_egp_folder)
                        if 'label' in node_info:
                            nodes_info[d['Element']['Log']['LinkFrom']] = Node(node_info['label'], task_id=d['Element']['Log']['LinkFrom'], info=node_info)

                    if d['Element']['Log']['LinkTo'] not in nodes_info:
                        node_info = _extract_node_info(d['Element']['Log']['LinkTo'], root, self.__unpacked_egp_folder)
                        if 'label' in node_info:
                            nodes_info[d['Element']['Log']['LinkTo']] = Node(node_info['label'], task_id=d['Element']['Log']['LinkTo'], info=node_info)
                    
                    if nodes_info[d['Element']['Log']['LinkTo']].parent != None:
                        if nodes_info[d['Element']['Log']['LinkFrom']].info['type'] in ['TASK']:
                            nodes_info[d['Element']['Log']['LinkTo']].parent = nodes_info[d['Element']['Log']['LinkFrom']]
#                        else:
#                            print("TRACE:1", nodes_info[d['Element']['Log']['LinkFrom']].info['type'])
                    else:
                        nodes_info[d['Element']['Log']['LinkTo']].parent = nodes_info[d['Element']['Log']['LinkFrom']]
            
            except KeyError as e:
                pass
            except Exception as e:
                print(f'EXCEPTION ({type(e).__name__}):'+str(e))
                print(traceback.format_exc())
                pass
        
        for import_task in self.__import_tasks:
#            print("TRACE:5", import_task)
            if import_task['input_id'] not in from_list: from_list.append(import_task['input_id'])
            if import_task['output_id'] not in to_list: to_list.append(import_task['output_id'])
            if import_task['id'] not in to_list: to_list.append(import_task['id'])
            
            if import_task['input_id'] not in nodes_info:
                node_info = _extract_node_info(import_task['input_id'], root, self.__unpacked_egp_folder)
                if 'label' in node_info:
                    nodes_info[import_task['input_id']] = Node(node_info['label'], task_id=import_task['input_id'], info=node_info)

            if import_task['output_id'] not in nodes_info:
                node_info = _extract_node_info(import_task['output_id'], root, self.__unpacked_egp_folder)
#                print("TRACE:7", node_info)
#                print("TRACE:8", import_task['output_id'])
                if 'label' in node_info:
                    nodes_info[import_task['output_id']] = Node(node_info['label'], task_id=import_task['output_id'], info=node_info)
            
            if import_task['id'] not in nodes_info:
                node_info = _extract_node_info(import_task['id'], root, self.__unpacked_egp_folder)
#                print("TRACE:9", node_info)
#                print("TRACE:10", import_task['id'])
                if 'label' in node_info:
                    nodes_info[import_task['id']] = Node(node_info['label'], task_id=import_task['id'], info=node_info)

            if nodes_info[import_task['output_id']].parent != None:
                if nodes_info[import_task['input_id']].info['type'] in ['TASK']:
                    nodes_info[import_task['output_id']].parent = nodes_info[import_task['input_id']]
#                else:
#                    print("TRACE:1", nodes_info[import_task['input_id']].info['type'])
            else:
                nodes_info[import_task['output_id']].parent = nodes_info[import_task['input_id']]
            
            if nodes_info[import_task['id']].parent != None:
                if nodes_info[import_task['input_id']].info['type'] in ['TASK']:
                    nodes_info[import_task['id']].parent = nodes_info[import_task['input_id']]
#                else:
#                    print("TRACE:1", nodes_info[import_task['input_id']].info['type'])
            else:
                nodes_info[import_task['id']].parent = nodes_info[import_task['input_id']]

        self.__nodes_info = nodes_info
#        _pp.pprint(self.__nodes_info)

        max_tree_size = 0
        for task_id in from_list:
            if task_id not in to_list and task_id in self.__nodes_info:
#                print("TRACE:4", task_id, self.__nodes_info[task_id].info['index'], self.__nodes_info[task_id].info['type'])
                if (not (self.__nodes_info[task_id].info['index'] == 0 and self.__nodes_info[task_id].info['type'] == 'SHORTCUT')
                    or self.manual_root_node_label == self.__nodes_info[task_id].info['label']): 
                    self.__root_id_list.append({ 'task_id': task_id, 'index':  self.__nodes_info[task_id].info['index']})
                else:
                    print("Skipping possible ROOT:"+str(self.__nodes_info[task_id]))
                
                tree_size = 0
                for pre, _, node in RenderTree(self.__nodes_info[task_id], style=AsciiStyle):
                    tree_size+=1
                    
                if tree_size > max_tree_size:
                    max_tree_size = tree_size
                    self.__main_root_id = task_id

#        print("ROOT ID:["+self.__main_root_id+']')
#        print("ROOT ID LIST:["+str(self.__root_id_list)+']')
        
    def nb_input_files(self):
        return (len(self.__import_tasks))

    def input_files_info(self, nb=0):
        root = self.__tree.getroot()
        
        if len(self.__import_tasks) > 0:
            return (self.__import_tasks[nb]['lib_name'], self.__import_tasks[nb]['table_name'], self.__import_tasks[nb]['full_path'], self.__import_tasks[nb]['column_mapping'])
        else:
            return (None, None, None, None)

    def print_main_project(self):
        if not self.__nodes_info: self.__scan_egp_file()
        for pre, _, node in RenderTree(self.__nodes_info[self.__main_root_id], style=AsciiStyle):
            print("%s%s" % (pre, node.name))

    def print_all_jobs(self):
        if not self.__nodes_info: self.__scan_egp_file()
#        _pp.pprint(self.__root_id_list)
        sorted_root_list = sorted(self.__root_id_list, key=itemgetter('index')) 
        for root_id in sorted_root_list:
            print("-------------------------------------")
            if self.manual_root_node_label:
    #            print("element_offset:"+str(self.__nodes_info[root_id['task_id']].info['index']))
                if self.__nodes_info[root_id['task_id']].info['label'] == self.manual_root_node_label:
                    for pre, _, node in RenderTree(self.__nodes_info[root_id['task_id']], style=AsciiStyle):
                        print("%s%s" % (pre, node.name))
            else:
                for pre, _, node in RenderTree(self.__nodes_info[root_id['task_id']]):
                    print("%s%s" % (pre, node.name))
        print("-------------------------------------")

    def get_list_of_tasks(self):
        if not self.__nodes_info: self.__scan_egp_file()
        list_of_tasks = []
        sorted_root_list = sorted(self.__root_id_list, key=itemgetter('index')) 
        
        for root_id in sorted_root_list:
            if self.manual_root_node_label:
                if self.__nodes_info[root_id['task_id']].info['label'] == self.manual_root_node_label:
                    for pre, _, node in RenderTree(self.__nodes_info[root_id['task_id']]):
                        list_of_tasks.append(node)
            else:
                for pre, _, node in RenderTree(self.__nodes_info[root_id['task_id']]):
                    list_of_tasks.append(node)
                
        return list_of_tasks

